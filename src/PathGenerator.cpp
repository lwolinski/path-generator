//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "PathGenerator/PathGenerator.h"

PathGenerator::PathGenerator( double BlendDist )
: BlendDist(BlendDist), TotalPathLength(0.0), PathLengthSoFar(0.0), CurrentSegment(0), NumberOfSegments(0)
{
	
}

PathGenerator::PathGenerator( const PathGenerator& other )
{
	BlendDist = other.BlendDist;
	
	TotalPathLength = 0.0;
	
	PathLengthSoFar = 0.0;
	
	CurrentSegment = 0.0;
	
	Points = other.Points;
	
	PathPreprocessor();
}

PathGenerator& PathGenerator::operator= ( const PathGenerator& other )
{
	if (&other == this)
	{
		return *this;
	}
	
	BlendDist = other.BlendDist;
	
	TotalPathLength = 0.0;
	
	PathLengthSoFar = 0.0;
	
	CurrentSegment = 0.0;
	
	Points = other.Points;
	
	PathPreprocessor();
	
	return *this;
}

PathGenerator::~PathGenerator()
{
}

void PathGenerator::AddPoints(const Eigen::MatrixXd& NewPoints)
{
	if (Points.cols() == 0)
	{
		Points = NewPoints;
	}
	else
	{
		Points.conservativeResize( Points.rows(), Points.cols() + NewPoints.cols() );
		Points.rightCols( NewPoints.cols() ) = NewPoints;
	}
	
	PathPreprocessor();
}

void PathGenerator::SetBlendDist( double NewBlendDist )
{
	BlendDist = NewBlendDist;
}

void PathGenerator::PathPreprocessor()
{
	//~ Compute the parameters defining all the path segments (linear and circular blend part)
	
	//~ T. Kunz and M. Stilman. “Time-Optimal Trajectory Generation for Path Following with Bounded Acceleration and Velocity”. In: 2012 Robotics: Science and Systems (RSS). July 2012
	
	NumberOfSegments = Points.cols() - 1;
	
	int SegmentIndex;
	Eigen::Vector3d StartPoint;
	if (StartPoints.cols() == 0)
	{
		SegmentIndex = 0;
		StartPoint = Points.col(SegmentIndex);
		StartPoints = Eigen::MatrixXd(3, NumberOfSegments);
		CenterPoints = Eigen::MatrixXd(3, NumberOfSegments);
		UnitYs = Eigen::MatrixXd(3, NumberOfSegments);
		UnitXs = Eigen::MatrixXd(3, NumberOfSegments);
		SegmentLength = Eigen::VectorXd(NumberOfSegments);
		Angle = Eigen::VectorXd(NumberOfSegments);
		BlendPart = Eigen::VectorXd(NumberOfSegments);
		Radius = Eigen::VectorXd(NumberOfSegments);
		
	}
	else
	{
		SegmentIndex = StartPoints.cols() - 1;
		StartPoint = StartPoints.col(SegmentIndex);
		StartPoints.conservativeResize(3, NumberOfSegments);
		CenterPoints.conservativeResize(3, NumberOfSegments);
		UnitYs.conservativeResize(3, NumberOfSegments);
		UnitXs.conservativeResize(3, NumberOfSegments);
		SegmentLength.conservativeResize(NumberOfSegments);
		Angle.conservativeResize(NumberOfSegments);
		BlendPart.conservativeResize(NumberOfSegments);
		Radius.conservativeResize(NumberOfSegments);
	}
	
	double SegmentLengthNext;
	
	Eigen::Vector3d UnitY, UnitYNext, UnitX, CenterPoint, EndPoint, EndPointNext;
	
	for (int i = SegmentIndex; i < NumberOfSegments; ++i)
	{
		EndPoint = Points.col(i + 1);
		SegmentLength(i) = (EndPoint - StartPoint).norm();
		UnitY = (EndPoint - StartPoint) / SegmentLength(i);
		
		if (i < NumberOfSegments - 1)
		{
			EndPointNext = Points.col(i + 2);
			SegmentLengthNext = (EndPointNext - EndPoint).norm();
			UnitYNext = (EndPointNext - EndPoint) / SegmentLengthNext;
			Angle(i) = acos(UnitY.transpose() * UnitYNext);
			
			if ( (Angle(i) > 0.0) && (BlendDist > 0.0) )
			{
				BlendPart(i) = ( Eigen::Vector3d( SegmentLength(i) / 2.0, SegmentLengthNext / 2.0, BlendDist * sin(Angle(i) / 2.0) / ( 1.0 - cos(Angle(i) / 2.0) ) ) ).minCoeff();
				Radius(i) = BlendPart(i) / tan(Angle(i) / 2.0);
				CenterPoint = EndPoint + (UnitYNext - UnitY) / (UnitYNext - UnitY).norm() * Radius(i) / cos(Angle(i) / 2.0);
				UnitX = (EndPoint - BlendPart(i) * UnitY - CenterPoint) / (EndPoint - BlendPart(i) * UnitY - CenterPoint).norm();
			}
			else
			{
				BlendPart(i) = 0.0;
				Radius(i) = 0.0;
				CenterPoint = EndPoint;
				UnitX = Eigen::Vector3d::Zero();
			}
		}
		else
		{
			//~ The last segment does not have a linear blend part
			Angle(i) = 0.0;
			BlendPart(i) = 0.0;
			Radius(i) = 0.0;
			CenterPoint = EndPoint;
			UnitX = Eigen::Vector3d::Zero();
		}
		
		StartPoints.col(i) = StartPoint;
		CenterPoints.col(i) = CenterPoint;
		UnitYs.col(i) = UnitY;
		UnitXs.col(i) = UnitX;
		SegmentLength(i) += -BlendPart(i) + Radius(i) * Angle(i);
		StartPoint = CenterPoint + Radius(i) * ( UnitX * cos(Angle(i)) + UnitY * sin(Angle(i)) );
	}
	
	TotalPathLength = SegmentLength.sum();
}
	
Eigen::VectorXd PathGenerator::GeneratePath( double PathPos )
{
	//~ “Trajectory Planning”. In: Robotics: Modelling, Planning and Control. London: Springer London, 2009, pp. 161–189. isbn: 978-1-84628-642-1. doi: 10.1007/978-1-84628-642-1_4 . url: https://doi.org/10.1007/978-1-84628-642-1_4

	//~ T. Kunz and M. Stilman. “Time-Optimal Trajectory Generation for Path Following with Bounded Acceleration and Velocity”. In: 2012 Robotics: Science and Systems (RSS). July 2012
	
	Eigen::VectorXd PosAndPosPrim(6);
	Eigen::Vector3d Pos;
	Eigen::Vector3d PosPrim;
	
	if ( ( (PathPos - PathLengthSoFar) > SegmentLength(CurrentSegment) ) && ( CurrentSegment < NumberOfSegments ) )
	{
		//~ Time for a new segment
		PathLengthSoFar += SegmentLength(CurrentSegment);
		++CurrentSegment;
	}
	
	int i;
	if (CurrentSegment < NumberOfSegments)
	{
		i = CurrentSegment;
	}
	else
	{
		i = NumberOfSegments - 1;
		//~ PathLengthSoFar -= SegmentLength(i); // ?
	}
	
	Eigen::Vector3d StartPoint = StartPoints.col(i);
	Eigen::Vector3d CenterPoint = CenterPoints.col(i);
	Eigen::Vector3d UnitY = UnitYs.col(i);
	Eigen::Vector3d UnitX = UnitXs.col(i);
	
	if (i < NumberOfSegments - 1)
	{
		//~ If this is not the last segment, then it has the linear part and the circular blend at the end leading to the next segment
		if ( (PathPos - PathLengthSoFar) <= ( SegmentLength(i) - Radius(i) * Angle(i) ) )
		{
			//~ Linear part
			Pos = StartPoint + UnitY * (PathPos - PathLengthSoFar);
			PosPrim = UnitY;
		}
		else
		{
			//~ Circular blend
			double PathCircular = PathPos - PathLengthSoFar - (SegmentLength(i) - Radius(i) * Angle(i));
			Pos = CenterPoint + Radius(i) * ( UnitX * cos( PathCircular / Radius(i) ) + UnitY * sin( PathCircular / Radius(i) ) );
			PosPrim = ( -UnitX * sin( PathCircular / Radius(i) ) + UnitY * cos( PathCircular / Radius(i) ) );
		}
	}
	else
	{
		//~ This is the last segment and it has only a linear part
		Pos = StartPoint + UnitY * (PathPos - PathLengthSoFar);
		PosPrim = UnitY;
	}
	
	PosAndPosPrim.head(3) = Pos;
	PosAndPosPrim.tail(3) = PosPrim;
	return PosAndPosPrim;
}

double PathGenerator::GetTotalPathLength()
{
	return TotalPathLength;
}

void PathGenerator::PrintSegmentLength()
{
	std::cout << "SegmentLegths:" << std::endl << SegmentLength << std::endl << "Total path length: " << TotalPathLength << std::endl;
}

//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#include "PathGenerator/PathGenerator.h"
#include <iostream>
#include <fstream>

void SaveResults(const std::string &OutputFileName, const Eigen::MatrixXd &Data)
{
	std::fstream OutFile;
	OutFile.open(OutputFileName, std::ios::out);
	if (OutFile.is_open()) {
		OutFile << Data;
		OutFile.close();
		std::cout << "File written." << std::endl;
	}
	else {
		std::cout << "Could not create file: " << OutputFileName << std::endl;
	}
}

int main()
{
	double BlendDist = 0.05;
	PathGenerator MyPath( BlendDist );
	
	Eigen::MatrixXd NewPoints(3,4);
	Eigen::Vector3d X1(0.56, 0.0, 0.89);
	Eigen::Vector3d X2(0.0, 0.2, 0.9);
	Eigen::Vector3d X3(0.0, -0.2, 0.9);
	Eigen::Vector3d X4(-0.5, -0.2, 0.9);
	Eigen::Vector3d X5(-0.5, 0.2, 0.9);
	Eigen::Vector3d X6(0.0, 0.2, 0.9);
	//~ Eigen::Vector3d X5(-0.5, -0.2, 1.9);
	//~ Eigen::Vector3d X6(-0.5, -0.2, 2.9);
	
	NewPoints.col(0) = X1;
	NewPoints.col(1) = X2;
	NewPoints.col(2) = X3;
	NewPoints.col(3) = X4;
	
	MyPath.AddPoints(NewPoints);
	
	MyPath.PrintSegmentLength();
	
	MyPath.AddPoints(X5);
	MyPath.AddPoints(X6);
	
	MyPath.PrintSegmentLength();
	
	double TotalPathLength = MyPath.GetTotalPathLength();
	
	int steps = 1001;
	
	double ds = TotalPathLength / (steps-1);
	
	Eigen::MatrixXd Results(steps, 7);
	
	double s = 0.0;
	
	for (int k = 0; k < steps; ++k)
	{
		Results(k, 0) = s;
		Results.block(k, 1, 1, 6) = MyPath.GeneratePath(s).transpose();
		s += ds;
	}
	
	SaveResults("../results/path.txt", Results);
	
	PathGenerator MyPath2(MyPath);
	
	PathGenerator MyPath3 = MyPath2;
	
	MyPath2.PrintSegmentLength();
	
	MyPath3.PrintSegmentLength();
	
	Eigen::MatrixXd Results2(steps, 7);
	
	s = 0.0;
	
	for (int k = 0; k < steps; ++k)
	{
		Results2(k, 0) = s;
		Results2.block(k, 1, 1, 6) = MyPath2.GeneratePath(s).transpose();
		s += ds;
	}
	
	SaveResults("../results/path2.txt", Results2);
	
	return 0;
}

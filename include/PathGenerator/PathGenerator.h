//~ Copyright (C) 2019 Łukasz Woliński
//~ You may use, distribute and modify this code under the terms of the BSD-3-Clause License.

#ifndef PATHGENERATOR_H_
#define PATHGENERATOR_H_

#include <iostream>
#include <Eigen/Dense>

class PathGenerator
{
public:
	PathGenerator( double BlendDist = 0.0 );
	
	PathGenerator( const PathGenerator& other);
	
	PathGenerator& operator= ( const PathGenerator& other );
	
	~PathGenerator();
	
	void AddPoints(const Eigen::MatrixXd& NewPoints);
	
	void SetBlendDist( double NewBlendDist );
	
	void PathPreprocessor();
	
	Eigen::VectorXd GeneratePath( double PathPos );
	
	double GetTotalPathLength();
	
	void PrintSegmentLength();
	
protected:
	Eigen::MatrixXd Points;
	Eigen::MatrixXd StartPoints;
	Eigen::MatrixXd CenterPoints;
	Eigen::MatrixXd UnitYs;
	Eigen::MatrixXd UnitXs;
	Eigen::VectorXd SegmentLength;
	Eigen::VectorXd Angle;
	Eigen::VectorXd BlendPart;
	Eigen::VectorXd Radius;
	double BlendDist;
	double TotalPathLength;
	double PathLengthSoFar;
	int CurrentSegment;
	int NumberOfSegments;
};

#endif /* PATHGENERATOR_H_ */
